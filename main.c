#include "tm4c123gh6pm.h"

// FUNCTION PROTOTYPES: Each subroutine defined
void Activate_Clock_To_Ports(void);
void Init_PortB(void);
void Init_PortF(void);
void Init_PortE(void);
void SysTick_Wait10ms(unsigned long delay);
void SysTick_Wait(unsigned long delay);
void SysTick_Init(void);
// ***** 3. Subroutines Section *****


typedef enum s_stats{
    S1,
    S2,
    S3,
    S4,
    S5,
    S6,
    S7,
    S8,
    S9,
    LAST
}s_stats;

struct stat{
    unsigned long car;
    unsigned long walk;
    unsigned long time;
    unsigned long next[8];
};

// east/west red light connected to PB5                         0x20
// east/west yellow light connected to PB4                  0x10
// east/west green light connected to PB3                       0x08
// north/south facing red light connected to PB2        0x04
// north/south facing yellow light connected to PB1 0x02
// north/south facing green light connected to PB0  0x01

// pedestrian detector connected to PE2 (1=pedestrian present)
// north/south car detector connected to PE1 (1=car present)
// east/west car detector connected to PE0 (1=car present)

// "walk" light connected to PF3 (built-in green LED)
// "don't walk" light connected to PF1 (built-in red LED)

struct stat FSM[LAST] = {
    {0x0C, 0x02, 50, {S1,S1,S2,S2,S2,S2,S2,S2}},//S1
    {0x14, 0x02, 50, {S1,S1,S3,S3,S5,S5,S3,S3}},//S2
    {0x21, 0x02, 50, {S3,S4,S3,S4,S4,S4,S4,S4}},//S3
    {0x22, 0x02, 50, {S3,S1,S3,S1,S5,S1,S5,S5}},//S4
    {0x24, 0x08, 50, {S5,S7,S7,S7,S5,S7,S7,S7}},//S5
    {0x24, 0x02, 50, {S6,S6,S6,S6,S6,S6,S6,S6}},//S6
    {0x24, 0x00, 50, {S8,S8,S8,S8,S8,S8,S8,S8}},//S7
    {0x24, 0x08, 50, {S9,S9,S9,S9,S9,S9,S9,S9}},//S8
    {0x24, 0x00, 50, {S1,S1,S3,S1,S5,S1,S3,S1}},//S9
};

int main(void){
    unsigned int s = 0;
    unsigned int next = 0;
    Activate_Clock_To_Ports();
  Init_PortB();
    Init_PortF();
    Init_PortE();
    SysTick_Init();

  while(1){
        GPIO_PORTB_DATA_R = FSM[s].car;
        GPIO_PORTF_DATA_R = FSM[s].walk;
        SysTick_Wait10ms(FSM[s].time);
        next = GPIO_PORTE_DATA_R;
        s = FSM[s].next[next];
  }
}


void Activate_Clock_To_Ports(void){
    volatile unsigned long delay;
    SYSCTL_RCGC2_R |= 0x32; // activate clock portB,portE,PortF
    delay = SYSCTL_RCGC2_R;
}

void Init_PortB(void){
    GPIO_PORTB_LOCK_R = 0x4c4f434b;
    GPIO_PORTB_CR_R = 0x3F; // changes PB0-PB5
    GPIO_PORTB_AMSEL_R = 0x00;
    GPIO_PORTB_PCTL_R = 0x00000000;
    GPIO_PORTB_DIR_R = 0x3F;
    GPIO_PORTB_AFSEL_R = 0x00000000;
    GPIO_PORTB_PUR_R = 0x00;
    GPIO_PORTB_DEN_R = 0x3F;
}

void Init_PortF(void){
    GPIO_PORTF_LOCK_R = 0x4c4f434b;
    GPIO_PORTF_CR_R = 0x0A; // changes PF1,PF3
    GPIO_PORTF_AMSEL_R = 0x00;
    GPIO_PORTF_PCTL_R = 0x00000000;
    GPIO_PORTF_DIR_R = 0x0A;
    GPIO_PORTF_AFSEL_R = 0x00000000;
    GPIO_PORTF_PUR_R = 0x00;
    GPIO_PORTF_DEN_R = 0x0A;
}

void Init_PortE(void){
    GPIO_PORTE_LOCK_R = 0x4c4f434b;
    GPIO_PORTE_CR_R = 0x07; // changes PE0-PE2
    GPIO_PORTE_AMSEL_R = 0x00;
    GPIO_PORTE_PCTL_R = 0x00000000;
    GPIO_PORTE_DIR_R = 0x00;
    GPIO_PORTE_AFSEL_R = 0x00000000;
    GPIO_PORTE_PUR_R = 0x00;
    GPIO_PORTE_DEN_R = 0x07;
}

void SysTick_Init(void){
  NVIC_ST_CTRL_R = 0;               // disable SysTick during setup
  NVIC_ST_CTRL_R = 0x00000005;      // enable SysTick with core clock
}

// The delay parameter is in units of the 80 MHz core clock. (12.5 ns)
void SysTick_Wait(unsigned long delay){
  NVIC_ST_RELOAD_R = delay-1;  // number of counts to wait
  NVIC_ST_CURRENT_R = 0;       // any value written to CURRENT clears
  while((NVIC_ST_CTRL_R&0x00010000)==0){ // wait for count flag
  }
}
// 800000*12.5ns equals 10ms
void SysTick_Wait10ms(unsigned long delay){
  unsigned long i;
  for(i=0; i<delay; i++){
    SysTick_Wait(800000);  // wait 10ms
  }
}















